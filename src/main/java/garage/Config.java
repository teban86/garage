package garage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@Scope("prototype")
public class Config {
	
	@Bean
	public Voiture voitureFactory() {
		return new Voiture();
	}
	@Bean
	public Voiture voitureFactory(String couleur, float poid) {
		return new Voiture(couleur,poid);
	}
	@Bean
	public Vehicule vehiculeFactory(String couleur, float poid) {
		return new Vehicule(couleur,poid);
	}
	@Bean
	public Vehicule vehiculeFactory() {
		return new Vehicule();
	}

}
